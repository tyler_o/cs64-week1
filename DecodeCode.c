//
//  DecodeCode.c
//  cs64week1
//
//  Created by Tyler Weimin Ouyang on 10/11/14.
//  Copyright (c) 2014 Tyler Weimin Ouyang. All rights reserved.
//
#include "DecodeCode.h"

int unsignedBitsNthroughM(int v, int start)
{
    // return the unsigned value in bits start through start+5
    switch (start) {
        case 0:
            return v & 0x03f;
        case 11:
            return (v & 0x01f000) >> 11;
        case 16:
            return (v & 0x01f0000) >> 16;
        case 21:
            return (v & 0x03e00000) >> 21;
        case 26:
            return (v & 0xfc000000) >> 26;
    }
    return v;
}


int signedBits0through15(int v)
{
    // return the signed value in bits 0 through 15
    if 	(v & 0x08000)
        v = v | 0xffff0000;
    else
        v = v & 0x0ffff;
    return v;
}

mipsinstruction decode(int value)
{
	mipsinstruction instr;

	// TODO: fill in the fields
    instr.opcode = unsignedBitsNthroughM(value, 26);
	instr.func = unsignedBitsNthroughM(value, 0);
	instr.imm = signedBits0through15(value);
	instr.rs = unsignedBitsNthroughM(value, 21);
	instr.rt = unsignedBitsNthroughM(value, 16);
	instr.rd = unsignedBitsNthroughM(value, 11);

	return instr;
}

