//
//  RandomCode.c
//  cs64week1
//
//  Created by Tyler Weimin Ouyang on 10/11/14.
//  Copyright (c) 2014 Tyler Weimin Ouyang. All rights reserved.
//
#include "stdio.h"
#include "stdlib.h"

int multiplyBy4(int v)
{
    // TODO: multiply the number by 4
    v = v << 2;
    return v;
}

int setBit7to1(int v)
{
    // TODO: set bit 7 to 1
    v = v | 0x080;
    return v;
}

int setBit5to0(int v)
{
    // TODO: set bit 5 to 0
    v = v & 0xfdf;
    return v;
}

int flipBit3(int v)
{
    // TODO: flip bit 3 (if it is 0, make it 1.  If 0, make 1)
    v = v ^ 0x08;
    return v;
}

int ifBit4is0(int v)
{
    // TODO: check to see if bit 4 is a 0 - return 1 if true, 0 if false
    if 	(v & 0x010)
        return 0;
    else
        return 1;
}

int ifBit7is1(int v)
{
    // check to see if bit 7 is a 1 - return 1 if true, 0 if false
    if    (v & 0x080)
        return 1;
    else
        return 0;
    return v;
}

int unsignedBits0through3(int v)
{
    // return the unsigned value in bits 0 through 3
    v = v & 0x0f;
    return v;
}

int unsignedBits4through9(int v)
{
    // return the unsigned value in bits 4 through 9
    v = (v & 0x03f0) >> 4;
    return v;
}

int signedBits0through3(int v)
{
    // return the signed value in bits 0 through 3
    if 	(v & 0x08)
        v = v | 0xfffffff0;
    else
        v = v & 0x0f;
    return v;
}

int signedBits4through9(int v)
{
    // return the signed value in bits 4 through 9
    if 	(v & 0x0200)
        v = (v >> 4) | 0xffffffc0;
    else
        v = (v >> 4) & 0x03f;
    return v ;
}

int setBits2through7(int v, int setValue)
{
    // set bits 2 through 7 in v to become setValue
    setValue = setValue << 2;
    v = v & 0xf03;
    v = v ^ setValue;
    return v;
}




